package com.example.demo.BookAuthorAPI;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @GetMapping("/book")
    public ArrayList<Book> getListBook(){
        ArrayList<Book> books = new ArrayList<>();

        Author author1 = new Author("Quyên" , 'F' ,"caiquanque@yahoo.com");
        Author author2 = new Author("Lan" , 'F' , "caidaubui@gmail.com");
        Author author3 = new Author("Hải" , 'M' , "threesome@facebook.com");
        
        Book book1 = new Book("Giang" , author1,344.00,21321);
        Book book2 = new Book("Thạnh" , author2,12312.00,29999);
        Book book3 = new Book("Ninh" , author3,42342.00,28888);  
        
        books.add(book1);
        books.add(book2);
        books.add(book3);

        return books ;
    }

   
}
